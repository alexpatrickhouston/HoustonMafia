﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace HoustonMafia.Models
{
    public class Game
    {
        [Key]
        [Required]
        public int GameID { get; set; }
        [Required]
        public string GameName { get; set; }
        [Required]
        public string OwnerID { get; set; }
        public string MostRecentlyDeceased { get; set; } = null;
        public string Password { get; set; }
        [Range(6, 100, ErrorMessage = "Must have 6 players or more")]
        public int Max_Players { get; set; } = 8;
        public Status GameStatus { get; set; } = (int)Status.Lobby;
        public Time State { get; set; } = (int)Time.Night;
    }
    public enum Status
    {
        Lobby = 0,
        InProgress = 1,
        MafiaVictory = 2,
        VillagerVictory = 3
    }
    public enum Time
    {
        Night =0,
        Day = 1
    }
}
