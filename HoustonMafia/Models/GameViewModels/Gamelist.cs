﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoustonMafia.Models.GameViewModels
{
    public class Gamelist
    {
        public int GameID;
        public string GameName;
        public bool Password;
        public int CurrentPlayerCount;
        public int MaxPlayers;
        public string OwnerID;
        public Status GameStatus;
    }
}
