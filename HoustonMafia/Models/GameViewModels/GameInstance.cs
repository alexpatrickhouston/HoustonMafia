﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoustonMafia.Models.GameViewModels
{
    public class GameInstance
    {
        public Game CurrentGame;
        public UserGame currentplayer;
        public List<UserGame> Players;
        public List<Message> GeneralChatLog;//Holds the messages for the game
        public GameInstance(Game game, UserGame cp, List<UserGame> ap,List<Message> am)
        {
            currentplayer = cp;
            CurrentGame = game;
            Players = ap;
            GeneralChatLog = am;
        }
    }
}
