﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HoustonMafia.Models
{
    public class UserGame
    { 
        public UserGame() { }
        //linking table for player and game
        [Key, ForeignKey("User")]
        public string UserID { get; set; }
        [Key, ForeignKey("Game")]
        public int GameID { get; set; }
        [Display(Name = "Nickname")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string Nickname { get; set; }
        [Range(-1, 3, ErrorMessage = "Invalid Role")]
        [Display(Name = "Role")]
        public Role Role { get; set; } = 0;
        public bool IsAlive { get; set; } = true;
        //Tells whether someone has voted or not(since people can only vote once per phase)
        public bool HasVoted { get; set; } = false;
        //Tells the current amount of votes that person has (either Mafia or General vote)
        public int VoteCount { get; set; } = 0;
        public bool Investigated { get; set; } = false;
    }
    public enum Role
    {
        Unset=-1,
        Mafia=0,
        Detective = 1,
        Villager = 2
    }
}
