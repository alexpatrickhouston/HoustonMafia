﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HoustonMafia.Models
{
    public class Message
    {
        public Message() { }
        //linking table for player and game
        [Key, ForeignKey("User")]
        public string UserID { get; set; }
        [Key, ForeignKey("Game")]
        public int GameID { get; set; }
        [Key]
        public DateTime Sent { get; set; }
        public string Content { get; set; }
    }
}
