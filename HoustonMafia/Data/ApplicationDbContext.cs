﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using HoustonMafia.Models;

namespace HoustonMafia.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<UserGame>().HasKey(k => new { k.GameID, k.UserID });
            builder.Entity<Message>().HasKey(k => new { k.GameID, k.UserID,k.Sent });
        }

        public DbSet<HoustonMafia.Models.Game> Game { get; set; }

        public DbSet<HoustonMafia.Models.UserGame> UserGame { get; set; }

        public DbSet<HoustonMafia.Models.Message> Message { get; set; }
    }
}
