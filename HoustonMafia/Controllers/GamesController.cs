﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HoustonMafia.Data;
using HoustonMafia.Models;
using HoustonMafia.Models.GameViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HoustonMafia.Controllers
{

    [Authorize]
    public class GamesController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;

        public GamesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }
        public IActionResult MyGames()
        {
            var mygames = GetUsersGames();
            return View(mygames);
        }
        public IActionResult Index()
        {
            var gamelist = GetPublicGames();
            return View(gamelist);
        }
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Game
                .SingleOrDefaultAsync(m => m.GameID == id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }
        [HttpGet]
        public IActionResult InGame(int gameid)//Method for rejoining a game
        {
            //if (gameid == 0)
            //    return NotFound();
            var uid = _userManager.GetUserId(User);
            GameInstance gameinstance = GetGameInstance(gameid, uid);
            if (gameinstance.CurrentGame.GameStatus == 0)//if the game is in lobby load lobby
                return RedirectToAction("Lobby", new { gameid = gameinstance.CurrentGame.GameID });
            return View("Ingame", gameinstance);
        }
        [HttpGet]
        public IActionResult Lobby(int gameid)
        {
            if (TempData.ContainsKey("PlayerCount"))
                ViewBag.Message = TempData["PlayerCount"].ToString();
            var uid = _userManager.GetUserId(User);
            GameInstance gameinstance = GetGameInstance(gameid, uid);
            return View("Lobby", gameinstance);
        }
        [HttpGet]
        public async Task<IActionResult> StartGame(int id)
        {//
            var uid = _userManager.GetUserId(User);
            GameInstance gameinstance = GetGameInstance(id, uid);
            int playercount = gameinstance.Players.Count();
            if (playercount < 6)
            {
                TempData["PlayerCount"] = "Not Enough Players Minimum 6 players required";
                return RedirectToAction("Lobby", new { gameid = id });
            }
            Random rand = new Random();
            List<UserGame> users = gameinstance.Players.ToList();
            int mafiacount = (playercount / 3);
            int detectivecount = (playercount / 6);
            int rolenumber = rand.Next(0, playercount - 1);
            do//Set the Mafia
            {
                if (users[rolenumber].Role == Role.Unset)
                {
                    users[rolenumber].Role = Role.Mafia;
                    mafiacount--;
                }
                rolenumber = rand.Next(0, playercount - 1);
            } while (mafiacount > 0);
            do
            {
                if (users[rolenumber].Role == Role.Unset)
                {
                    users[rolenumber].Role = Role.Detective;
                    mafiacount--;
                }
                rolenumber = rand.Next(0, playercount - 1);
            } while (detectivecount > 0);
            foreach (UserGame u in users)
            {
                if (u.Role == Role.Unset)
                {
                    u.Role = Role.Villager;
                }
            }
            gameinstance.CurrentGame.GameStatus = Status.InProgress;
            await _context.SaveChangesAsync();
            return RedirectToAction("Ingame", new { gameid = id });
        }
        public IActionResult Create()
        {
            return View();
        }
        public IActionResult Join(string ID)
        {
            ViewBag.GameID = ID;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Join([Bind("UserID,GameID,Nickname,Role,IsAlive")]UserGame ugame)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ugame);
                await _context.SaveChangesAsync();
                return RedirectToAction("InGame", new { gameid = ugame.GameID });
            }
            return RedirectToAction("Join");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GameID,OwnerID,GameName,Password,Max_Players")] Game game)
        {
            if (ModelState.IsValid)
            {
                _context.Add(game);
                await _context.SaveChangesAsync();
                return RedirectToAction("Join", new { id = game.GameID });
            }
            return View(game);
        }
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Game.SingleOrDefaultAsync(m => m.GameID == id);
            if (game == null)
            {
                return NotFound();
            }
            return View(game);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GameID,GameName,Password,Max_Players")] Game game)
        {
            if (id != game.GameID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(game);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GameExists(game.GameID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(game);
        }
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Game
                .SingleOrDefaultAsync(m => m.GameID == id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var game = await _context.Game.SingleOrDefaultAsync(m => m.GameID == id);
            _context.Game.Remove(game);
            var ugames = _context.UserGame.Where(m => m.GameID == id).ToList();
            foreach (UserGame u in ugames)
            {
                _context.UserGame.Remove(u);
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        private bool GameExists(int id)
        {
            return _context.Game.Any(e => e.GameID == id);
        }
        private IEnumerable<Gamelist> GetUsersGames()//Gets active games the user is currently in
        {
            var gamelist = new List<Gamelist>();

            foreach (var usergame in _context.UserGame)
            {

                if (_userManager.GetUserId(User) == usergame.UserID)
                {
                    var game = _context.Game.Find(usergame.GameID);
                    int gameid = game.GameID;
                    int playercount = _context.UserGame.Count(t => t.GameID == gameid);
                        gamelist.Add(MakeGamelist(game, playercount));
                }

            }
            return gamelist;

        }
        private GameInstance GetGameInstance(int gameid, string playerid)
        {
            Game currentgame = _context.Game.Find(gameid);//Grab the game off the ID;
            List<UserGame> allplayers = _context.UserGame.Where(g => g.GameID == gameid).ToList();//Grab all the players involved
            List<Message> GeneralChat = _context.Message.Where(g => g.GameID == gameid).ToList();//Grab all the Messages saved in chat

            GeneralChat.Sort((x, y) => DateTime.Compare(x.Sent, y.Sent));//Sort the List to make sure the messages are in order when sent to the view
            UserGame currentplayer = allplayers.Find(g => g.UserID == playerid);
            return new GameInstance(currentgame, currentplayer, allplayers, GeneralChat);


        }
        [HttpPost]
        public async Task<int> DayAction(string player, string GG)
        {

            int votecount = 0;//Get a vote count
            int alivecount = 0;//Get a count of number of players alive
            int votemax = 0;////For finding max
            UserGame PlayerToBeKilled = null;//For the player to be killed
            GameInstance game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());//Get current game state
            if (game.currentplayer.HasVoted == true)//If they have already voted just return the gamestate
                return 1;
            else
                game.currentplayer.HasVoted = true;//Else they are now voting
            foreach (var p in game.Players)
            {//Go through all the players
                if (p.Nickname == player)//If you have found the player with the same name
                    p.VoteCount++;//Increase their vote
                if (p.HasVoted)//If they have voted
                    votecount++;//increment the count
                if (p.IsAlive)//Figure out how many people are alive
                    alivecount++;
                if (p.VoteCount > votemax)
                {
                    votemax = p.VoteCount;
                    PlayerToBeKilled = p;
                }


            }
            game.CurrentGame.State = Time.Night;//It is how night time
            if (alivecount == votecount)//Everyone still alive has voted
            {
                //Change the state
                game.CurrentGame.State = Time.Night;//It is how night time
                //Find out who needs to be hung for their crimes
                //Need to find a way to get count of number of times that element appears in the user game table for this game

                //Two cases either 1 person got highest votes or more than one did
                //If more than one had highest votes then the vote does nothing, no one dies
                //Reset the the votecounts for everyone
                int samevotecount = 0;
                foreach (var x in game.Players)
                {
                    if (x.VoteCount == votemax)
                        samevotecount++;
                    x.VoteCount = 0;//reset for the next phase
                    x.HasVoted = false;
                }
                if (samevotecount > 1)//If there is no majority
                {
                    game.CurrentGame.MostRecentlyDeceased = null;//No one dies
                }
                else
                {//else that person is declared dead
                    game.CurrentGame.MostRecentlyDeceased = PlayerToBeKilled.Nickname;
                    PlayerToBeKilled.IsAlive = false;//And dies
                }
                int gamestatus =CheckGameOver(game);
                await _context.SaveChangesAsync();
                game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());
                if (gamestatus == 1)//villager victory
                    return 3;
                if (gamestatus == 2)//mafia victory
                    return 4;
                return 0;
            }
            await _context.SaveChangesAsync();
            game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());
            return 2;
        }
        [HttpPost]
        public async Task<int> MafiaAction(string player, string GG)
        {

            int votecount = 0;//Get a vote count
            int alivecount = 0;//Get a count of number of players alive
            int votemax = 0;////For finding max
            UserGame PlayerToBeKilled = null;//For the player to be killed
            GameInstance game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());//Get current game state
            if (game.currentplayer.HasVoted == true)//If they have already voted just return the gamestate
                return 1;
            else
                game.currentplayer.HasVoted = true;//Else they are now voting
            foreach (var p in game.Players)
            {//Go through all the players
                if (p.Nickname == player)//If you have found the player with the same name
                    p.VoteCount++;//Increase their vote
                if (p.HasVoted)//If they have voted
                    votecount++;//increment the count
                if (p.IsAlive && (p.Role == Role.Mafia || p.Role == Role.Detective))//Figure out how many people are alive
                    alivecount++;
                if (p.VoteCount > votemax)
                {
                    votemax = p.VoteCount;
                    PlayerToBeKilled = p;
                }


            }

            if (alivecount == votecount)//Everyone still alive has voted
            {
                //Change the state
                game.CurrentGame.State = Time.Day;//It is how night time
                //Find out who needs to be hung for their crimes
                //Need to find a way to get count of number of times that element appears in the user game table for this game

                //Two cases either 1 person got highest votes or more than one did
                //If more than one had highest votes then the vote does nothing, no one dies
                //Reset the the votecounts for everyone
                int samevotecount = 0;
                foreach (var x in game.Players)
                {
                    if (x.VoteCount == votemax)
                        samevotecount++;
                    x.VoteCount = 0;//reset for the next phase
                    x.HasVoted = false;
                }
                if (samevotecount > 1)//If there is no majority
                {
                    game.CurrentGame.MostRecentlyDeceased = null;//No one dies
                }
                else
                {//else that person is declared dead
                    game.CurrentGame.MostRecentlyDeceased = PlayerToBeKilled.Nickname;
                    PlayerToBeKilled.IsAlive = false;//And dies
                }
                int gamestatus = CheckGameOver(game);
                await _context.SaveChangesAsync();
                game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());
                if (gamestatus == 1)//villager victory
                    return 3;
                if (gamestatus == 2)//mafia victory
                    return 4;
                return 0;
            }
            await _context.SaveChangesAsync();
            game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());
            return 2;
        }
        [HttpPost]
        public async Task<int> DetectiveAction(string player, string GG)
        {

            int votecount = 0;//Get a vote count
            int alivecount = 0;//Get a count of number of players alive
            int votemax = 0;////For finding the largest
            UserGame PlayerToBeKilled = null;//For the player to be killed
            GameInstance game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());//Get current game state
            if (game.currentplayer.HasVoted == true)//If they have already voted just return the gamestate
                return 1;
            else
                game.currentplayer.HasVoted = true;//Else they are now voting
            foreach (var p in game.Players)
            {//Go through all the players
                if (p.Nickname == player)//If you have found the player with the same name
                    p.Investigated = true;//Increase their vote
                if (p.HasVoted)//If they have voted
                    votecount++;//increment the count
                if (p.IsAlive && (p.Role == Role.Mafia || p.Role == Role.Detective))//Figure out how many people are alive
                {
                    alivecount++;
                }
                if (p.VoteCount > votemax)
                {
                    votemax = p.VoteCount;
                    PlayerToBeKilled = p;
                }


            }

            if (alivecount == votecount)//Everyone still alive has voted
            {
                //Change the state
                game.CurrentGame.State = Time.Day;//It is how night time
                //Find out who needs to be hung for their crimes
                //Need to find a way to get count of number of times that element appears in the user game table for this game

                //Two cases either 1 person got highest votes or more than one did
                //If more than one had highest votes then the vote does nothing, no one dies
                //Reset the the votecounts for everyone
                int samevotecount = 0;
                foreach (var x in game.Players)
                {
                    if (x.VoteCount == votemax)
                        samevotecount++;
                    x.VoteCount = 0;//reset for the next phase
                    x.HasVoted = false;
                }
                if (samevotecount >= 1)//If there is no majority
                {
                    game.CurrentGame.MostRecentlyDeceased = null;//No one dies
                }
                else
                {//else that person is declared dead
                    game.CurrentGame.MostRecentlyDeceased = PlayerToBeKilled.Nickname;
                    PlayerToBeKilled.IsAlive = false;//And dies
                }
                int gamestatus = CheckGameOver(game);
                await _context.SaveChangesAsync();
                game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());
                if (gamestatus == 1)//villager victory
                    return 3;
                if (gamestatus == 2)//mafia victory
                    return 4;
                return 0;
            }
            await _context.SaveChangesAsync();
            game = GetGameInstance(Convert.ToInt32(GG), _userManager.GetUserId(User).ToString());
            return 2;
        }
        [HttpPost]
        public async Task CreateMessage(string message, string GG)
        {
            DateTime current = DateTime.Now;
            string[] parts = message.Split(new char[] { ':' }, 2);//Should return array with first item being username and second being content
            string username = parts[0];//First half
            string content = parts[1];//Second half
            int GID = Convert.ToInt32(GG);//Convert the game id back to an int (since html is dumb and makes me make things text)
            Message tba = new Message
            {
                UserID = username,
                GameID = GID,
                Content = content,
                Sent = current
            };

            _context.Add(tba);
            await _context.SaveChangesAsync();
        }
        private int CheckGameOver(GameInstance game)
        {
            int playersalive = 0;
            int MafiaAlive = 0;
            foreach (var p in game.Players)
            {
                if (p.IsAlive)
                {
                    playersalive++;
                    if (p.Role == Role.Mafia)
                        MafiaAlive++;
                }
            }
            if (MafiaAlive == 0)//if the mafia is all dead
            {
                game.CurrentGame.GameStatus = Status.VillagerVictory;//Set the victory condition
                return 1;//Villager Victory
            }
            if (playersalive <= (MafiaAlive * 2))//if Mafia is half the population or more
            {
                game.CurrentGame.GameStatus = Status.VillagerVictory;
                return 2;//Mafia Victory
            }
                return 0;//Games not over
        }
        private IEnumerable<Gamelist> GetPublicGames()//Get active joinable games 
        {
            var gamelist = new List<Gamelist>();

            foreach (var game in _context.Game)
            {
                int gameid = game.GameID;
                int playercount = _context.UserGame.Count(t => t.GameID == gameid);
                UserGame usergame = _context.UserGame.Find(gameid, _userManager.GetUserId(User).ToString());
                if (usergame == null && game.GameStatus == Status.Lobby && playercount <= game.Max_Players)
                    //Player cant already be in the game
                    //game must be in lobby state meaning it hasnt started
                    //Game must not be at capacity
                    gamelist.Add(MakeGamelist(game, playercount));

            }
            return gamelist;

        }
        private Gamelist MakeGamelist(Game game, int Playercount)
        {
            bool password = false;
            if (game.Password != null)
                password = true;
            return new Gamelist
            {
                GameName = game.GameName,
                GameID = game.GameID,
                Password = password,
                CurrentPlayerCount = Playercount,
                MaxPlayers = game.Max_Players,
                OwnerID = game.OwnerID,
                GameStatus = game.GameStatus
            };
        }
    }
}
